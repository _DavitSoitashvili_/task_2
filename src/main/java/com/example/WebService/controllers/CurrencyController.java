package com.example.WebService.controllers;

import com.example.WebService.models.Converter;
import com.example.WebService.models.Currency;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/currency")
public class CurrencyController {
    private final Currency[] currencies = new Currency[]{
            new Currency("USD", 3.16, 3.2),
            new Currency("EUR", 3.425, 3.475),
            new Currency("GBP", 3.84, 3.94),
            new Currency("RUB", 4.12, 4.3),
            new Currency("TRY", 0.4, 0.47),
            new Currency("AZN", 1.6, 1.84)
    };

    @GET
    @Path("/currencies")
    @Produces(MediaType.APPLICATION_JSON)
    public Currency[] currencies() {
        return currencies;
    }

    @POST
    @Path("/buy")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Converter buyCurrency(Converter converter) {

        for (Currency currency : currencies) {
            if (converter.getTo().equals(currency.getTitle())) {
                return new Converter(converter.getTo(), (converter.getAmount() / currency.getSell()));
            }
        }

        return null;
    }

    @POST
    @Path("/sell")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Converter sellCurrency(Converter converter) {

        for (Currency currency : currencies) {
            if (converter.getTo().equals(currency.getTitle())) {
                return new Converter(converter.getTo(), (currency.getBuy() * converter.getAmount()));
            }
        }

        return null;
    }
}
