package com.example.WebService.models;

public class Converter {
    private String to;
    private Double amount;

    public Converter() {

    }

    public Converter(String to, Double amount) {
        this.to = to;
        this.amount = amount;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
